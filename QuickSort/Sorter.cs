﻿using System;

// ReSharper disable InconsistentNaming
#pragma warning disable SA1611

namespace QuickSort
{
    public static class Sorter
    {
        /// <summary>
        /// Sorts an <paramref name="array"/> with quick sort algorithm.
        /// </summary>
        public static void QuickSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            array.QuickSort(0, array.Length - 1);
        }

        public static void QuickSort(this int[] array, int startIndex, int endIndex)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length <= 1)
            {
                return;
            }

            int[] stack = new int[endIndex - startIndex + 1];
            int topIndex = 0;

            stack[topIndex] = startIndex;
            stack[++topIndex] = endIndex;

            while (topIndex >= 0)
            {
                endIndex = stack[topIndex--];
                startIndex = stack[topIndex--];

                int partitionIndex = IterativePartitionIndex(array, startIndex, endIndex);

                if (partitionIndex - 1 > startIndex)
                {
                    stack[++topIndex] = startIndex;
                    stack[++topIndex] = partitionIndex - 1;
                }

                if (partitionIndex + 1 < endIndex)
                {
                    stack[++topIndex] = partitionIndex + 1;
                    stack[++topIndex] = endIndex;
                }
            }
        }

        public static int IterativePartitionIndex(this int[] array, int startIndex, int endIndex)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int pivot = array[startIndex];
            int index = endIndex;

            for (int j = endIndex; j > startIndex; j--)
            {
                if (array[j] > pivot)
                {
                    (array[index], array[j]) = (array[j], array[index]);
                    index--;
                }
            }

            (array[index], array[startIndex]) = (array[startIndex], array[index]);
            return index;
        }

        /// <summary>
        /// Sorts an <paramref name="array"/> with recursive quick sort algorithm.
        /// </summary>
        public static void RecursiveQuickSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            array.RecursiveQuickSort(0, array.Length - 1);
        }

        public static void RecursiveQuickSort(this int[] array, int startIndex, int endIndex)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (startIndex < endIndex)
            {
                int partitionIndex = array.RecursivePartitionIndex(startIndex, endIndex);
                array.RecursiveQuickSort(startIndex, partitionIndex - 1);
                array.RecursiveQuickSort(partitionIndex + 1, endIndex);
            }
        }

        public static int RecursivePartitionIndex(this int[] array, int startIndex, int endIndex)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int pivot = array[startIndex];
            int index = endIndex;

            array.RecursionComparing(startIndex, pivot, ref index, endIndex);

            (array[index], array[startIndex]) = (array[startIndex], array[index]);
            return index;
        }

        public static void RecursionComparing(this int[] array, int startIndex, int pivot, ref int pivotIndex, int index)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (index <= startIndex)
            {
                return;
            }

            if (array[index] > pivot)
            {
                (array[pivotIndex], array[index]) = (array[index], array[pivotIndex]);
                pivotIndex--;
            }

            index--;

            RecursionComparing(array, startIndex, pivot, ref pivotIndex, index);
        }
    }
}
